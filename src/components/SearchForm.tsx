import React from 'react';

// Material-UI
import { Button, Card, CardContent, Grid, Typography } from '@mui/material';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';

// Hook
import { useSearchInfo } from '../hooks/serach_form';

/**
 * 検索フォーム
 * @returns
 */
const SearchForm: React.FC = () => {
  // 検索フォーム　Hook
  const { handleChangeStartDate, handleChangeFinishDate, handleSubmit } =
    useSearchInfo();
  return (
    <Card variant="outlined" style={{ marginBottom: '50px' }}>
      <CardContent>
        <Typography gutterBottom variant="h5">
          検索条件
        </Typography>
        <Grid container spacing={1}>
          {/* 開始日　入力フォーム */}
          <Grid item>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DatePicker
                label="開始日"
                format="YYYY/MM/DD"
                onChange={handleChangeStartDate}
              />
            </LocalizationProvider>
          </Grid>
          {/* 終了日　入力フォーム */}
          <Grid item>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DatePicker
                label="終了日"
                format="YYYY/MM/DD"
                onChange={handleChangeFinishDate}
              />
            </LocalizationProvider>
          </Grid>
          {/* 検索ボタン */}
          <Grid item xs={12}>
            <Button
              size="large"
              type="submit"
              variant="contained"
              fullWidth
              onClick={handleSubmit}
            >
              検索
            </Button>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default SearchForm;
