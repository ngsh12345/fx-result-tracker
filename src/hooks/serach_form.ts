import React from 'react';
import { useRecoilState } from 'recoil';

// State
import {
  allTradeHistoryState,
  currencyPairListState,
  searchInfoState,
  tradeHistoryState,
  tradeSummaryState
} from '../states/trade';

// Hook Util
import {
  extractUniqueCurrencyPairs,
  filterTradeHistoryForDate,
  getTradeSummary
} from './util';

/**
 * 検索フォーム用 Hook
 * @returns
 */
export const useSearchInfo = () => {
  // 検索フォーム　入力値　State
  const [searchInfo, setSearchInfo] = useRecoilState(searchInfoState);
  // 全トレード履歴　State
  const [allTradeHistory] = useRecoilState(allTradeHistoryState);
  //現在表示しているトレード履歴　State
  const [, setTradeHistory] = useRecoilState(tradeHistoryState);
  // 現在表示しているトレードサマリ　State
  const [, setTradeSummary] = useRecoilState(tradeSummaryState);
  // 現在表示している通貨ペア一覧　State
  const [, setCurrencyPairList] = useRecoilState(currencyPairListState);

  /**
   * 開始日のonChange用関数
   */
  const handleChangeStartDate = React.useCallback(
    (value: any) => {
      setSearchInfo({
        startDate: value.format('YYYY/MM/DD 00:00'),
        finishDate: searchInfo.finishDate
      });
    },
    [searchInfo.finishDate, setSearchInfo]
  );

  /**
   * 終了日のonChange用関数
   */
  const handleChangeFinishDate = React.useCallback(
    (value: any) => {
      setSearchInfo({
        startDate: searchInfo.startDate,
        finishDate: value.format('YYYY/MM/DD 23:59')
      });
    },
    [searchInfo.startDate, setSearchInfo]
  );

  /**
   * 検索ボタンのonClick用関数
   */
  const handleSubmit = React.useCallback(() => {
    // 入力された開始日、終了日でトレード履歴を絞り込む
    const filterdTradeHistory = filterTradeHistoryForDate(
      allTradeHistory,
      searchInfo.startDate,
      searchInfo.finishDate
    );

    // トレード履歴、トレードサマリ、通貨ペア一覧を更新
    setTradeHistory(filterdTradeHistory);
    setTradeSummary(getTradeSummary(filterdTradeHistory));
    setCurrencyPairList(extractUniqueCurrencyPairs(filterdTradeHistory));
  }, [
    allTradeHistory,
    searchInfo.finishDate,
    searchInfo.startDate,
    setCurrencyPairList,
    setTradeHistory,
    setTradeSummary
  ]);

  return {
    searchInfo,
    handleChangeStartDate,
    handleChangeFinishDate,
    handleSubmit
  };
};
