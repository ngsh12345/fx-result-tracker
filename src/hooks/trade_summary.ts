import { useRecoilState } from 'recoil';

// State
import { tradeHistoryState, tradeSummaryState } from '../states/trade';

/**
 * トレードサマリ用　Hook
 * @returns
 */
export const useTradeSummary = () => {
  // 現在表示しているトレード履歴　State
  const [tradeHistory] = useRecoilState(tradeHistoryState);
  // 現在表示しているトレードサマリ　State
  const [tradeSummary] = useRecoilState(tradeSummaryState);

  return { tradeHistory, tradeSummary };
};
