import React from 'react';
import { useRecoilState } from 'recoil';

// Api
import { TradeHistory, getTradeHistory } from '../apis/spreadsheet';

// State
import {
  tradeHistoryState,
  tradeSummaryState,
  currencyPairListState,
  allTradeHistoryState
} from '../states/trade';

// Hook Util
import {
  extractUniqueCurrencyPairs,
  filterTradeHistoryForDate,
  getTradeSummary
} from './util';

/**
 * トレード履歴用　Hook
 * @param startDate 開始日
 * @param finishDate 終了日
 * @returns
 */
export const useTradeHistory = (startDate?: string, finishDate?: string) => {
  // 現在表示しているトレード履歴 State
  const [tradeHistory, setTradeHistory] = useRecoilState(tradeHistoryState);
  // 全トレード履歴 State
  const [, setAllTradeHistory] = useRecoilState(allTradeHistoryState);
  // 現在表示しているトレードサマリ State
  const [tradeSummary, setTradeSummary] = useRecoilState(tradeSummaryState);
  // 現在表示している通貨ペア一覧 State
  const [, setCurrencyPairList] = useRecoilState(currencyPairListState);

  React.useEffect(() => {
    // スプレッドシートからトレード履歴を取得
    getTradeHistory((data: TradeHistory[]) => {
      // 初期値の開始日、終了日で絞り込む
      const tradeHistory = filterTradeHistoryForDate(
        data,
        startDate,
        finishDate
      );
      // トレードサマリを取得
      const tradeSummary = getTradeSummary(data);

      // トレード履歴、トレードサマリ、通貨ペア一覧を更新
      setAllTradeHistory(tradeHistory);
      setTradeHistory(tradeHistory);
      setTradeSummary(tradeSummary);
      setCurrencyPairList(extractUniqueCurrencyPairs(data));
    });
  }, [
    startDate,
    finishDate,
    setTradeHistory,
    setTradeSummary,
    setCurrencyPairList,
    setAllTradeHistory
  ]);

  return { tradeHistory, tradeSummary };
};
