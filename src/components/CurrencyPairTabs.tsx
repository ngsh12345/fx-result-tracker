import React from 'react';

// Material-UI
import { Box, Tab, Tabs } from '@mui/material';

// Hook
import { useTab } from '../hooks/tab';

/**
 * 通貨ペア切り替え用のタブ
 * @returns
 */
const CurrencyPairTabs: React.FC = () => {
  const { tab, currencyPairList, handleChangeTab } = useTab({
    index: 0,
    name: '全通貨ペア'
  });

  return (
    <Box
      style={{
        display: 'flex',
        justifyContent: 'center',
        paddingTop: '20px'
      }}
      sx={{ borderBottom: 1, borderColor: 'divider', marginBottom: '30px' }}
    >
      <Tabs
        value={tab.index}
        onChange={handleChangeTab}
        aria-label="basic tabs example"
      >
        {currencyPairList.map((tabName, index) => {
          return <Tab key={tabName} label={tabName} value={index} />;
        })}
      </Tabs>
    </Box>
  );
};

export default CurrencyPairTabs;
