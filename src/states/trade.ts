import { atom } from 'recoil';
import { TradeHistory } from '../apis/spreadsheet';

/** トレードサマリ */
interface TradeSummary {
  totalCount: number;
  profitCount: number;
  losscutCount: number;
  winningRate: number;
  totalAmount: number;
  totalProfitAmount: number;
  totalLosscutAmount: number;
}

/**
 * 全トレード履歴
 */
export const allTradeHistoryState = atom<TradeHistory[]>({
  key: 'allTradeHistoryState',
  default: []
});

/**
 * 現在表示しているトレード履歴
 */
export const tradeHistoryState = atom<TradeHistory[]>({
  key: 'tradeHistoryState',
  default: []
});

/**
 * 現在表示しているトレードサマリ
 */
export const tradeSummaryState = atom<TradeSummary>({
  key: 'tradeSummaryState',
  default: {
    totalCount: 0,
    profitCount: 0,
    losscutCount: 0,
    winningRate: 0,
    totalAmount: 0,
    totalProfitAmount: 0,
    totalLosscutAmount: 0
  }
});

/**
 * 現在表示している通貨ペア一覧
 */
export const currencyPairListState = atom<string[]>({
  key: 'currencyPairListState',
  default: ['全通貨ペア']
});

interface SearchInfo {
  startDate: string;
  finishDate: string;
}

/**
 * 現在入力した開始日、終了日
 */
export const searchInfoState = atom<SearchInfo>({
  key: 'searchInfoState',
  default: {
    startDate: '',
    finishDate: ''
  }
});
