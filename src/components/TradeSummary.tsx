import React from 'react';

// Material-UI
import { Box, Card, CardContent, Grid, Typography } from '@mui/material';

// Hook
import { useTradeSummary } from '../hooks/trade_summary';

// Util
import { formatAmountWithCurrency } from '../utils/util';

/**
 * トレードサマリ
 * @returns
 */
const TradeSummary: React.FC = () => {
  const { tradeSummary } = useTradeSummary();
  return (
    <Box style={{ marginBottom: '50px' }}>
      <Grid container spacing={5}>
        {/* 総トレード数 */}
        <Grid item>
          <Card variant="outlined" style={{ minWidth: '150px' }}>
            <CardContent>
              <Typography
                gutterBottom
                variant="h6"
                style={{
                  color: '#1976d2',
                  fontSize: '1rem',

                  textAlign: 'center'
                }}
              >
                総トレード数
              </Typography>
              <p
                style={{
                  fontSize: '1.5rem',
                  textAlign: 'center',
                  fontWeight: 'bold'
                }}
              >
                {tradeSummary.totalCount}回
              </p>
            </CardContent>
          </Card>
        </Grid>
        {/* 勝ちトレード数 */}
        <Grid item>
          <Card variant="outlined" style={{ minWidth: '150px' }}>
            <CardContent>
              <Typography
                gutterBottom
                variant="h6"
                style={{
                  color: '#1976d2',
                  fontSize: '1rem',

                  textAlign: 'center'
                }}
              >
                勝ちトレード数
              </Typography>
              <p
                style={{
                  fontSize: '1.5rem',
                  textAlign: 'center',
                  fontWeight: 'bold'
                }}
              >
                {tradeSummary.profitCount}回
              </p>
            </CardContent>
          </Card>
        </Grid>
        {/* 負けトレード数 */}
        <Grid item>
          <Card variant="outlined" style={{ minWidth: '150px' }}>
            <CardContent>
              <Typography
                gutterBottom
                variant="h6"
                style={{
                  color: '#1976d2',
                  fontSize: '1rem',

                  textAlign: 'center'
                }}
              >
                負けトレード数
              </Typography>
              <p
                style={{
                  fontSize: '1.5rem',
                  textAlign: 'center',
                  fontWeight: 'bold'
                }}
              >
                {tradeSummary.losscutCount}回
              </p>
            </CardContent>
          </Card>
        </Grid>
        {/* 勝率 */}
        <Grid item>
          <Card variant="outlined" style={{ minWidth: '150px' }}>
            <CardContent>
              <Typography
                gutterBottom
                variant="h6"
                style={{
                  color: '#1976d2',
                  fontSize: '1rem',

                  textAlign: 'center'
                }}
              >
                勝率
              </Typography>
              <p
                style={{
                  fontSize: '1.5rem',
                  textAlign: 'center',
                  fontWeight: 'bold'
                }}
              >
                {tradeSummary.winningRate}%
              </p>
            </CardContent>
          </Card>
        </Grid>
        {/* 合計損益額 */}
        <Grid item>
          <Card variant="outlined" style={{ minWidth: '150px' }}>
            <CardContent>
              <Typography
                gutterBottom
                variant="h6"
                style={{
                  color: '#1976d2',
                  fontSize: '1rem',

                  textAlign: 'center'
                }}
              >
                合計損益額
              </Typography>
              <p
                style={{
                  fontSize: '1.5rem',
                  textAlign: 'center',
                  fontWeight: 'bold',
                  color: tradeSummary.totalAmount < 0 ? 'red' : 'black'
                }}
              >
                {formatAmountWithCurrency(tradeSummary.totalAmount)}
              </p>
            </CardContent>
          </Card>
        </Grid>
        {/* 合計利益額 */}
        <Grid item>
          <Card variant="outlined" style={{ minWidth: '150px' }}>
            <CardContent>
              <Typography
                gutterBottom
                variant="h6"
                style={{
                  color: '#1976d2',
                  fontSize: '1rem',

                  textAlign: 'center'
                }}
              >
                合計利益額
              </Typography>
              <p
                style={{
                  fontSize: '1.5rem',
                  textAlign: 'center',
                  fontWeight: 'bold'
                }}
              >
                {formatAmountWithCurrency(tradeSummary.totalProfitAmount)}
              </p>
            </CardContent>
          </Card>
        </Grid>
        {/* 合計損失額 */}
        <Grid item>
          <Card variant="outlined" style={{ minWidth: '150px' }}>
            <CardContent>
              <Typography
                gutterBottom
                variant="h6"
                style={{
                  color: '#1976d2',
                  fontSize: '1rem',

                  textAlign: 'center'
                }}
              >
                合計損失額
              </Typography>
              <p
                style={{
                  fontSize: '1.5rem',
                  textAlign: 'center',
                  fontWeight: 'bold'
                }}
              >
                {formatAmountWithCurrency(tradeSummary.totalLosscutAmount)}
              </p>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Box>
  );
};

export default TradeSummary;
