import React from 'react';

// Material-UI
import {
  Box,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow
} from '@mui/material';

// Hook
import { useTradeHistory } from '../hooks/trade_history';

// Util
import { formatAmountWithCurrency } from '../utils/util';

/**
 * トレード履歴
 * @returns
 */
const TradeHistory: React.FC = () => {
  const { tradeHistory } = useTradeHistory();
  return (
    <Box>
      <TableContainer component={Paper} sx={{ maxHeight: 440 }}>
        <Table
          stickyHeader
          sx={{ minWidth: 650 }}
          size="small"
          aria-label="sticky table"
        >
          <TableHead>
            <TableRow>
              <TableCell>トレード日時</TableCell>
              <TableCell align="right">通貨ペア</TableCell>
              <TableCell align="right">売/買</TableCell>
              <TableCell align="right">決済損益</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {tradeHistory.map((elem) => (
              <TableRow
                key={elem.id}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                style={{ background: elem.amount < 0 ? '#d9989c' : 'none' }}
              >
                <TableCell component="th" scope="row">
                  {elem.datetime}
                </TableCell>
                <TableCell align="right">{elem.currencyPair}</TableCell>
                <TableCell align="right">{elem.tradeDirection}</TableCell>
                <TableCell align="right">
                  {formatAmountWithCurrency(elem.amount)}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};

export default TradeHistory;
