/**
 * 金額に３桁毎にカンマ、先頭に円マークを付与する
 * @param amount 金額
 * @returns
 */
export const formatAmountWithCurrency = (amount: number) => {
  return '¥' + amount.toLocaleString('ja-JP');
};
