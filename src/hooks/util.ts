// Api
import { TradeHistory } from '../apis/spreadsheet';

/**
 * 開始日～終了日内のトレード履歴を絞り込む
 * @param data トレード履歴
 * @param startDate 開始日
 * @param finishDate 終了日
 * @returns
 */
export const filterTradeHistoryForDate = (
  data: TradeHistory[],
  startDate?: string,
  finishDate?: string
) => {
  if (startDate && finishDate) {
    return data.filter((elem) => {
      const start = new Date(startDate);
      const finish = new Date(finishDate);
      const target = new Date(elem.datetime);
      return (
        start.getTime() <= target.getTime() &&
        target.getTime() <= finish.getTime()
      );
    });
  }

  return data;
};

/**
 * 選択した通貨ペアののトレード履歴を絞り込む
 * @param data トレード履歴
 * @param tabName 通貨ペア名
 * @returns
 */
export const filterTradeHistoryForTab = (
  data: TradeHistory[],
  tabName: string
) => {
  if (tabName === '全通貨ペア') {
    return data;
  }

  return data.filter((elem) => {
    return elem.currencyPair === tabName;
  });
};

/**
 * トレード履歴からトレードサマリを計算する
 * @param data トレード履歴
 * @returns
 */
export const getTradeSummary = (data: TradeHistory[]) => {
  const result = {
    totalCount: 0,
    profitCount: 0,
    losscutCount: 0,
    winningRate: 0,
    totalAmount: 0,
    totalProfitAmount: 0,
    totalLosscutAmount: 0
  };
  // 総トレード数
  result.totalCount = data.length;

  // 勝ちトレード数
  result.profitCount = data.filter((element) => element.amount >= 0).length;
  // 負けトレード数
  result.losscutCount = data.filter((element) => element.amount < 0).length;

  // 勝率
  if (result.totalCount !== 0) {
    // ゼロ除算を防止するため、0を返す
    result.winningRate = parseFloat(
      ((result.profitCount / result.totalCount) * 100).toFixed(1)
    );
  }

  // 合計損益額
  result.totalAmount = data.reduce(
    (acc, currentValue) => acc + currentValue.amount,
    0
  );

  // 合計利益額
  result.totalProfitAmount = data.reduce((accumulator, currentValue) => {
    if (currentValue.amount >= 0) {
      return accumulator + currentValue.amount;
    } else {
      return accumulator;
    }
  }, 0);

  // 合計損失額
  result.totalLosscutAmount = data.reduce((accumulator, currentValue) => {
    if (currentValue.amount < 0) {
      return accumulator + currentValue.amount;
    } else {
      return accumulator;
    }
  }, 0);

  return result;
};

/**
 * トレード履歴内に含まれる通貨ペアの一覧を出す
 * @param data トレード履歴
 * @returns
 */
export const extractUniqueCurrencyPairs = (data: TradeHistory[]) => {
  const uniquePairs = ['全通貨ペア'];
  const seenPairs = new Set();

  for (const item of data) {
    if (!seenPairs.has(item.currencyPair)) {
      uniquePairs.push(item.currencyPair);
      seenPairs.add(item.currencyPair);
    }
  }

  return uniquePairs;
};
