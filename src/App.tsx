import React from 'react';
import { RecoilRoot } from 'recoil';

// CSS
import './App.css';

// Component
import Home from './pages/Home';

const App: React.FC = () => {
  return (
    <RecoilRoot>
      <Home />
    </RecoilRoot>
  );
};

export default App;
