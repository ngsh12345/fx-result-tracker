import axios from 'axios';

/** Google Spread Sheet API URL */
const BASE_URL = `https://sheets.googleapis.com/v4/spreadsheets/${process.env.REACT_APP_GOOGLE_SPREAD_SHEET_ID}`;
/** APIキー（必須パラメータ） */
const QUERY_PARAMS = { key: process.env.REACT_APP_GOOGLE_API_KEY };
/** 行の上限 */
const LIMIT_ROW = 100000;

/** API取得後の成形したJSON型 */
export type TradeHistory = {
  /** ID */
  id: number;
  /** トレード日時 */
  datetime: string;
  /** 通貨ペア */
  currencyPair: string;
  /** 売-買 */
  tradeDirection: string;
  /** 決済損益*/
  amount: number;
};

/**
 * スプレッドシートのデータを取得する
 * @param successCallback 取得成功時のコールバック関数
 * @param failCallback 取得失敗時のコールバック関数
 */
export const getTradeHistory = async (
  /** 取得成功時のコールバック関数 */
  successCallback: Function,
  /** 取得失敗時のコールバック関数 */
  failCallback?: Function
) => {
  /** スプレッドシートの取得範囲セル */
  const dmmfx_range = `A1:N${LIMIT_ROW}`;
  const ig_range = `A1:P${LIMIT_ROW}`;

  try {
    // DMMFXとIGのデータを取得
    const requestList: any = [
      axios.get(`${BASE_URL}/values/DMMFX!${dmmfx_range}`, {
        params: QUERY_PARAMS
      }),
      axios.get(`${BASE_URL}/values/IG!${ig_range}`, {
        params: QUERY_PARAMS
      })
    ];

    // 全てのリクエストが完了するまで待機
    const responseList = await Promise.all(requestList);

    const result: TradeHistory[] = [];

    let count = 1;
    // DMMFXのデータ編集
    responseList[0].data.values.forEach((elem: any, i: number) => {
      if (i > 0) {
        result.push({
          id: count,
          datetime: elem[9],
          currencyPair: elem[0],
          tradeDirection: elem[1] === '買' ? '売' : '買',
          amount: Number(elem[7].replace(/,/g, ''))
        });

        count += 1;
      }
    });

    // IGのデータ編集
    responseList[1].data.values.forEach((elem: any, i: number) => {
      if (i > 0) {
        result.push({
          id: count,
          datetime: formatDateTime(elem[13]),
          currencyPair: elem[2].match(/[A-Z]{3}\/[A-Z]{3}/)[0],
          tradeDirection: getTradeDirectionForIG(
            Number(elem[7]),
            Number(elem[8]),
            Number(elem[11].replace(/,/g, ''))
          ),
          amount: Number(elem[11].replace(/,/g, ''))
        });

        count += 1;
      }
    });

    // 日時の早い順に並び替え
    result.sort((a, b) => {
      const dateA: Date = new Date(a.datetime);
      const dateB: Date = new Date(b.datetime);
      return dateA.getTime() - dateB.getTime();
    });

    successCallback(result);
  } catch (error) {
    if (failCallback) {
      failCallback(error);
    }
  }
};

/**
 * YYYY/MM/DD HH:MM形式で返却する
 * @param inputDate 日時の文字列
 * @returns YYYY/MM/DD HH:MM形式
 */
const formatDateTime = (inputDate: string) => {
  // "-" を "/" に置き換える
  const splitedSpaceList = inputDate.split(' ');
  const front = splitedSpaceList[0].replace(/-/g, '/');
  const backList = splitedSpaceList[1].split(':');

  return `${front} ${backList[0]}:${backList[1]}`;
};

/**
 * 売りトレードか買いトレードか判断する
 * @param openLevel 開始したレート
 * @param closeLevel 終了したレート
 * @param amount 決済金額
 * @returns
 */
const getTradeDirectionForIG = (
  openLevel: number,
  closeLevel: number,
  amount: number
): string => {
  if (
    (openLevel < closeLevel && amount > 0) ||
    (openLevel >= closeLevel && amount < 0)
  ) {
    return '買';
  } else {
    return '売';
  }
};
