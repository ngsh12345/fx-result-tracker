import React from 'react';

// Material-UI
import { Container } from '@mui/material';

// Component
import SearchForm from '../components/SearchForm';
import CurrencyPairTabs from '../components/CurrencyPairTabs';
import TradeSummary from '../components/TradeSummary';
import TradeHistory from '../components/TradeHistory';

const Home: React.FC = () => {
  return (
    <Container
      maxWidth="md"
      style={{ paddingTop: '50px', paddingBottom: '50px' }}
    >
      {/* 検索フォーム */}
      <SearchForm />
      {/* 通貨ペア選択用のタブ一覧 */}
      <CurrencyPairTabs />
      {/* トレードサマリ */}
      <TradeSummary />
      {/* トレード履歴 */}
      <TradeHistory />
    </Container>
  );
};

export default Home;
