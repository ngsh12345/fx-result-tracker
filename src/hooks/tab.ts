import React, { useState } from 'react';
import { useRecoilState } from 'recoil';

// State
import {
  allTradeHistoryState,
  currencyPairListState,
  tradeHistoryState,
  tradeSummaryState
} from '../states/trade';

// Hook Util
import { filterTradeHistoryForTab, getTradeSummary } from './util';

/** タブ情報 */
interface TabInfo {
  index: number;
  name: string;
}

/**
 * タブ用　Hook
 */
export const useTab = (tabInfo: TabInfo) => {
  // 選択したタブ　State
  const [tab, setTab] = useState<TabInfo>(tabInfo);
  // 現在表示している通貨ペア一覧　State
  const [currencyPairList] = useRecoilState(currencyPairListState);
  // 現在表示しているトレードサマリ　State
  const [, setTradeSummary] = useRecoilState(tradeSummaryState);
  // 全トレード履歴　State
  const [allTradeHistory] = useRecoilState(allTradeHistoryState);
  // 現在表示しているトレード履歴　State
  const [, setTradeHistory] = useRecoilState(tradeHistoryState);

  /**
   * タブ変更時onChange用関数
   */
  const handleChangeTab = React.useCallback(
    (event: any, value: number) => {
      const currentTabInfo = {
        index: value,
        name: currencyPairList[value]
      };

      // 選択した通貨ペアでトレード履歴を絞り込む
      const filterdTradeHistory = filterTradeHistoryForTab(
        allTradeHistory,
        currencyPairList[value]
      );

      // トレード履歴、トレードサマリ、現在のタブを更新
      setTab(currentTabInfo);
      setTradeHistory(filterdTradeHistory);
      setTradeSummary(getTradeSummary(filterdTradeHistory));
    },
    [allTradeHistory, currencyPairList, setTradeHistory, setTradeSummary]
  );

  return { tab, currencyPairList, handleChangeTab };
};
